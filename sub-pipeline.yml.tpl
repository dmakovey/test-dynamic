trigger_@@CHART@@:
  trigger:
    include: .gitlab-ci.yml
    strategy: depend
  variables:
    GITLAB_CHART_VERSION: @@CHART@@
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      when: manual
